/**
 * File: symtable.h
 * ----------- 
 *  Header file for Symbol table implementation.
 */

#ifndef _H_symtable
#define _H_symtable

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "ast.h"
#include "ast_decl.h"
#include "irgen.h"

using namespace std;

struct Data {
	Decl* decl;
	llvm::Value* val;
	bool isGlobal;
};

class SymbolTable
{
    protected:
        vector <map <string, struct Data*> > *scopesVec; //pointer to vector of maps
        int currScopeCnt;      //keep track of current scope

    public: 
        SymbolTable();        //Constructor
        void pushScope();   //Push map scope onto vector of maps
        void popScope();    //Pop map scope off vector of maps
        void addSymbol(string, Decl*, llvm::Value*);	//Add symbol to map in current scope
        struct Data* lookupCurr(string);      //Lookup symbol on map of current scope
        struct Data* lookupAll(string);    //Lookup symbol on entire vector
        void printTable();  //Print contents of symtable
		bool isGlobal(string);
};

#endif
