/**
 * File: irgen.h
 * -----------
 *  This file defines a class for LLVM IR Generation.
 *
 *  All LLVM instruction related functions or utilities can be implemented
 *  here. You'll need to customize this class heavily to provide any helpers
 *  or untility as you need.
 */

#ifndef _H_IRGen
#define _H_IRGen

// LLVM headers
#include "llvm/IR/Module.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"

class Type;

class IRGenerator {
  public:
    IRGenerator();
    ~IRGenerator();

    llvm::Module   *GetOrCreateModule(const char *moduleID);
    llvm::LLVMContext *GetContext() const { return context; }

    // Add your helper functions here
    llvm::Function *GetFunction() const;
    void SetFunction(llvm::Function *func);

    llvm::BasicBlock *GetBasicBlock() const;
    void SetBasicBlock(llvm::BasicBlock *bb);

	llvm::BasicBlock *GetFooter() const;
	void SetFooter(llvm::BasicBlock *fbb);

	llvm::BasicBlock *GetContinue() const;
	void SetContinue(llvm::BasicBlock *cbb);


    llvm::Type *GetIntType() const;
    llvm::Type *GetInt64Type() const;
    llvm::Type *GetBoolType() const;
    llvm::Type *GetFloatType() const;

	//convert function
	llvm::Type *Convert(Type *astTy);

  private:
    llvm::LLVMContext *context;
    llvm::Module      *module;

    // track which function or basic block is active
    llvm::Function    *currentFunc;
    llvm::BasicBlock  *currentBB;
	llvm::BasicBlock  *currentFooter;
	llvm::BasicBlock  *currentContinue;


    static const char *TargetTriple;
    static const char *TargetLayout;
};

#endif

