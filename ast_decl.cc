/* File: ast_decl.cc
 * -----------------
 * Implementation of Decl node classes.
 */
#include "ast_decl.h"
#include "ast_type.h"
#include "ast_stmt.h"
#include "symtable.h"
#include <string>
#include <iostream>        
         
Decl::Decl(Identifier *n) : Node(*n->GetLocation()) {
    Assert(n != NULL);
    (id=n)->SetParent(this); 
}

VarDecl::VarDecl(Identifier *n, Type *t, Expr *e) : Decl(n) {
    Assert(n != NULL && t != NULL);
    (type=t)->SetParent(this);
    if (e) (assignTo=e)->SetParent(this);
    typeq = NULL;
}

VarDecl::VarDecl(Identifier *n, TypeQualifier *tq, Expr *e) : Decl(n) {
    Assert(n != NULL && tq != NULL);
    (typeq=tq)->SetParent(this);
    if (e) (assignTo=e)->SetParent(this);
    type = NULL;
}

VarDecl::VarDecl(Identifier *n, Type *t, TypeQualifier *tq, Expr *e) : Decl(n) {
    Assert(n != NULL && t != NULL && tq != NULL);
    (type=t)->SetParent(this);
    (typeq=tq)->SetParent(this);
    if (e) (assignTo=e)->SetParent(this);
}
  
void VarDecl::PrintChildren(int indentLevel) { 
   if (typeq) typeq->Print(indentLevel+1);
   if (type) type->Print(indentLevel+1);
   if (id) id->Print(indentLevel+1);
   if (assignTo) assignTo->Print(indentLevel+1, "(initializer) ");
}

llvm::Value* VarDecl::Emit() {	
	//convert type and get name, id, etc.
	llvm::Module *mod = Node::irgen->GetOrCreateModule("Name_the_Module.bc");

	llvm::Type *ty = Node::irgen->Convert(this->type);

	char* name = this->GetIdentifier()->GetName();
	string id = string(name);

	llvm::BasicBlock* bb = irgen->GetBasicBlock();

	llvm::Value* val;
	//check if there is assignTo
	if( assignTo != NULL ) {
		val = this->assignTo->Emit();
	}
	else {
		val = llvm::Constant::getNullValue(ty);
	}

	if( symtab->isGlobal(id) ) {
		//cast val to constant*
		llvm::Constant* vall = llvm::cast<llvm::Constant>(val);

		//create new global
		llvm::GlobalVariable* gv = new llvm::GlobalVariable(
		/*module*/ *mod,
		/*type*/ ty,
		/*isConstant*/ false,
		/*linkage*/ llvm::GlobalValue::ExternalLinkage, 
		/*value*/ llvm::Constant::getNullValue(ty),
		/*id*/ name);

		//add symbol to table
		symtab->addSymbol(id, this, gv);
	}

	else {
		//alloca and store for local var
		llvm::AllocaInst* aloc = new llvm::AllocaInst(ty, name, bb);
		if(assignTo != NULL){
			llvm::StoreInst* str = new llvm::StoreInst(val, aloc, false, bb);
		}
		//add symbol to table
		symtab->addSymbol(id, this, aloc);
	}

	return NULL;
}

FnDecl::FnDecl(Identifier *n, Type *r, List<VarDecl*> *d) : Decl(n) {
    Assert(n != NULL && r!= NULL && d != NULL);
    (returnType=r)->SetParent(this);
    (formals=d)->SetParentAll(this);
    body = NULL;
    returnTypeq = NULL;
}

FnDecl::FnDecl(Identifier *n, Type *r, TypeQualifier *rq, List<VarDecl*> *d) : Decl(n) {
    Assert(n != NULL && r != NULL && rq != NULL&& d != NULL);
    (returnType=r)->SetParent(this);
    (returnTypeq=rq)->SetParent(this);
    (formals=d)->SetParentAll(this);
    body = NULL;
}

void FnDecl::SetFunctionBody(Stmt *b) { 
    (body=b)->SetParent(this);
}

void FnDecl::PrintChildren(int indentLevel) {
    if (returnType) returnType->Print(indentLevel+1, "(return type) ");
    if (id) id->Print(indentLevel+1);
    if (formals) formals->PrintAll(indentLevel+1, "(formals) ");
    if (body) body->Print(indentLevel+1, "(body) ");
}

llvm::Value* FnDecl::Emit() {
	//get mod
	llvm::Module *mod = irgen->GetOrCreateModule("blah.bc");

	//get return type
	llvm::Type* ty = irgen->Convert(this->returnType);
	char* name = this->GetIdentifier()->GetName();
	string id = string(name);
	llvm::Value* val = llvm::Constant::getNullValue(ty);

	//add to symtab
	symtab->addSymbol(id, this, val);
	//push scope
	symtab->pushScope();
	
	//get all arg types and add to vector
	std::vector<llvm::Type *> argTypes;
	int i;
	for(i = 0; i < this->GetFormals()->NumElements(); i++)
	{
		llvm::Type* argTy;
		argTy = irgen->Convert(this->GetFormals()->Nth(i)->GetType());
		char* name = this->GetFormals()->Nth(i)->GetIdentifier()->GetName();
		argTypes.push_back(argTy);
	}

	//create function type and function
	llvm::ArrayRef<llvm::Type *> argArray(argTypes);
	llvm::FunctionType* funcTy = llvm::FunctionType::get(ty, argArray, false);
	llvm::Function* f = llvm::cast<llvm::Function>(mod->getOrInsertFunction(name, funcTy));

	//get context and create bb for func
	llvm::LLVMContext *context = irgen->GetContext();
	llvm::BasicBlock *bb = llvm::BasicBlock::Create(*context, "entry", f);

	//set the current bb and func to this function's bb
	irgen->SetFunction(f);
	irgen->SetBasicBlock(bb);

	//loop over args and set names
	llvm::Function::arg_iterator args = f->arg_begin();
	for(i = 0; i < this->GetFormals()->NumElements(); i++)
	{
		char* argName = this->GetFormals()->Nth(i)->GetIdentifier()->GetName();
		llvm::Value* arg = args++;
		arg->setName(argName);
		this->GetFormals()->Nth(i)->Emit();
		llvm::Value* aloc = (symtab->lookupAll(argName))->val;
		llvm::StoreInst* str = new llvm::StoreInst(arg, aloc, false, bb);

	}

	//now we can loop over the stmts and emit
	StmtBlock* sb = dynamic_cast<StmtBlock *>(this->body);
	List<Stmt*> *stmtss = sb->GetStmts();
	if( stmtss->NumElements() > 0)
	{
		for ( int i = 0; i < stmtss->NumElements(); ++i ) {
        	Stmt *s = stmtss->Nth(i);
       		s->Emit();
      }
	}

	
	if( irgen->GetBasicBlock()->getTerminator() == NULL ) {
		llvm::UnreachableInst* ui = new llvm::UnreachableInst(*context,irgen->GetBasicBlock());
	}

	//popScope
	symtab->popScope();

	mod->dump();

	return NULL;
}

