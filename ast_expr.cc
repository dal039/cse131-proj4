/* File: ast_expr.cc
 * -----------------
 * Implementation of expression node classes.
 */

#include <string.h>
#include "ast_expr.h"
#include "ast_type.h"
#include "ast_decl.h"
#include "symtable.h"

#include <string>
#include <iostream>

#define INTEGER 0
#define FLOAT 1


llvm::Value* IntConstant::Emit()
{
    return llvm::ConstantInt::get(irgen->GetIntType(), value);
}

llvm::Value* FloatConstant::Emit()
{
    return llvm::ConstantFP::get(irgen->GetFloatType(), value);
}

llvm::Value* BoolConstant::Emit()
{
    return llvm::ConstantInt::get(irgen->GetBoolType(), value);
}

llvm::Value* VarExpr::Emit()
{
    //val from symtable
    llvm::Value* val = (symtab->lookupAll(string(id->GetName())))->val;
	/*if(symtab->lookupAll(id->GetName()) == NULL )
	{
		val = llvm::ConstantInt::get(irgen->GetIntType(), 1);
	}*/
    llvm::Value* loadedval = new llvm::LoadInst(val, id->GetName(), 
                                        irgen->GetBasicBlock());
    return loadedval;
}

llvm::Value* ArithmeticExpr::Emit()
{
    //Grab the operator
    Operator *opr = this->op;

    //Declare variables
    llvm::Value* lhs;
    llvm::Value* rhs;
    llvm::Type* lty;
    llvm::Type* rty;

    //Grab the rhs and lhs values. Make sure there is a lhs value to grab 
    //If there are lhs and rhs values get their types
    //Then check if those types are integer or float types
    if(left != NULL)
    {
        lhs = this->left->Emit();
        lty = lhs->getType();
    }
    rhs = this->right->Emit(); 
    rty = rhs->getType();

    //Set rhsType to INTEGER or FLOAT for unary operations
    /*bool rhsType;
    if (rty->isIntegerTy())
    {
        rhsType = INTEGER;
    }
    else if (rty->isFloatTy())
    {
        rhsType = FLOAT;
    }*/
    
	bool isInt = false;
	bool isFloat = false;

    if(left != NULL)
    {
        isInt = lty->isIntegerTy() && rty->isIntegerTy();
        isFloat = lty->isFloatTy() && rty->isFloatTy();
    }

    FieldAccess* rfa = dynamic_cast<FieldAccess*> (this->right);    

    /*** RETURNING llvm::VALUE ***/
    //Unary Operations
    if (left == NULL)
    {
        if (rty->isIntegerTy())
        {
            if (opr->IsOp("+"))
            {
                return rhs; 
            }
            else if (opr->IsOp("-"))
            {
                lhs = llvm::ConstantInt::get(rty, 0);
                return llvm::BinaryOperator::CreateSub(lhs, rhs , "-", 
                                                irgen->GetBasicBlock()); 
            }
            else if (opr->IsOp("++"))
            {
                lhs = llvm::ConstantInt::get(rty, 1);
                llvm::Value* inc = llvm::BinaryOperator::CreateAdd(rhs, lhs, opr->GetToken(), 
                                                irgen->GetBasicBlock());
				VarExpr* rightt = dynamic_cast<VarExpr *>(this->right); 
                llvm::Value* var = symtab->lookupAll(rightt->GetIdentifier()->GetName())->val;
                new llvm::StoreInst(inc, var, irgen->GetBasicBlock());
                return inc;
            }
            else if (opr->IsOp("--"))
            {
                lhs = llvm::ConstantInt::get(rty, 1);
                llvm::Value* dec = llvm::BinaryOperator::CreateSub(rhs, lhs, opr->GetToken(), 
                                                irgen->GetBasicBlock());
				VarExpr* rightt = dynamic_cast<VarExpr *>(this->right);
                llvm::Value* var = symtab->lookupAll(rightt->GetIdentifier()->GetName())->val;
                new llvm::StoreInst(dec, var, irgen->GetBasicBlock());
                return dec;
            }
        }
        else if (rty->isFloatTy() || rty->isVectorTy())
        {
            
            if (opr->IsOp("+"))
            {
                return rhs; 
            }
            else if (opr->IsOp("-"))
            {
                lhs = llvm::ConstantFP::get(rty, 0.0);
                return llvm::BinaryOperator::CreateFSub(lhs, rhs, opr->GetToken(), 
                                                irgen->GetBasicBlock());
            }
            else if (opr->IsOp("++"))
            {
                lhs = llvm::ConstantFP::get(rty, 1.0);
                llvm::Value* inc = llvm::BinaryOperator::CreateFAdd(rhs, lhs, opr->GetToken(), 
                                                irgen->GetBasicBlock());
				VarExpr* rightt;
                if (rfa == NULL)
                {
                    rightt = dynamic_cast<VarExpr*>(this->right);
                }
                else
                {
                    rightt = dynamic_cast<VarExpr*>(rfa->getBase());
                }
                llvm::Value* var = symtab->lookupAll(rightt->GetIdentifier()->GetName())->val;
                new llvm::StoreInst(inc, var, irgen->GetBasicBlock());
                return inc;    
            }
            else if (opr->IsOp("--"))
            {
                lhs = llvm::ConstantFP::get(rty, 1.0);
                llvm::Value* dec = llvm::BinaryOperator::CreateFSub(rhs, lhs, opr->GetToken(), 
                                                irgen->GetBasicBlock());
				VarExpr* rightt;
                if (rfa == NULL)
                {
                    rightt = dynamic_cast<VarExpr*>(this->right);
                }
                else
                {
                    rightt = dynamic_cast<VarExpr*>(rfa->getBase());
                }
                llvm::Value* var = symtab->lookupAll(rightt->GetIdentifier()->GetName())->val;
                new llvm::StoreInst(dec, var, irgen->GetBasicBlock());
                return dec;
            }       
        }
    }

    //Binary Operations
    if(isInt)
    {
        if(opr->IsOp("+"))
        {
            return llvm::BinaryOperator::CreateAdd(lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
        }
        else if(opr->IsOp("-"))
        {
            return llvm::BinaryOperator::CreateSub(lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
        }
        else if(opr->IsOp("*"))
        {
            return llvm::BinaryOperator::CreateMul(lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
        }
        else if(opr->IsOp("/"))
        {
            return llvm::BinaryOperator::CreateSDiv(lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
        }
    }
    else if(isFloat)
    {
        if(opr->IsOp("+"))
        {
            return llvm::BinaryOperator::CreateFAdd(lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
        }
        else if(opr->IsOp("-"))
        {
            return llvm::BinaryOperator::CreateFSub(lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
        }
        else if(opr->IsOp("*"))
        {
            return llvm::BinaryOperator::CreateFMul(lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
        }
        else if(opr->IsOp("/"))
        {
            return llvm::BinaryOperator::CreateFDiv(lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
        }
    }
    else if (lty->isVectorTy() || rty->isVectorTy())
    {
        if(lty->isFloatTy() || rty->isFloatTy())
        {
            llvm::Type* ty = lhs->getType();
            llvm::VectorType* vecTy = llvm::cast<llvm::VectorType>(ty);
            llvm::Constant* floatr;
            llvm::Constant* newVec;
            if(lty->isFloatTy())
            {
                floatr = llvm::cast<llvm::Constant>(lhs);
                newVec = llvm::ConstantVector::getSplat(vecTy->getNumElements(), floatr);
                lhs = dynamic_cast<llvm::Value*>(newVec);
            }
            else
            {
                floatr = llvm::cast<llvm::Constant>(rhs);
                newVec = llvm::ConstantVector::getSplat(vecTy->getNumElements(), floatr);
                rhs = dynamic_cast<llvm::Value*>(newVec);
            }
        }

        if(opr->IsOp("+"))
        {
            return llvm::BinaryOperator::CreateFAdd(lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
        }
        else if(opr->IsOp("-"))
        {
            return llvm::BinaryOperator::CreateFSub(lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
        }
        else if(opr->IsOp("*"))
        {
            return llvm::BinaryOperator::CreateFMul(lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
        }
        else if(opr->IsOp("/"))
        {
            return llvm::BinaryOperator::CreateFDiv(lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
        }
    }
}

llvm::Value* RelationalExpr::Emit()
{
    //Grab the operator
    Operator* opr = this->op;

    //Initialize Variables
    llvm::Value* lhs = this->left->Emit();
    llvm::Value* rhs = this->right->Emit(); 
    llvm::Type* lty = lhs->getType(); 
    llvm::Type* rty = rhs->getType();
    
    //Check if we're comparing ints or floats
    bool isInt = lty->isIntegerTy() && rty->isIntegerTy();
    bool isFloat = lty->isFloatTy() && rty->isFloatTy();

    llvm::CmpInst::OtherOps iop = llvm::CmpInst::ICmp;
    llvm::CmpInst::OtherOps fop = llvm::CmpInst::FCmp;

    llvm::CmpInst::Predicate pred;

    if (isInt)
    {
        if (opr->IsOp("<"))
        {
            pred = llvm::ICmpInst::ICMP_SLT;
        }
        else if (opr->IsOp("<="))
        {
            pred = llvm::ICmpInst::ICMP_SLE;
        }
        else if (opr->IsOp(">"))
        {
            pred = llvm::ICmpInst::ICMP_SGT;
        }
        else if (opr->IsOp(">="))
        {
            pred = llvm::ICmpInst::ICMP_SGE;
        }
        return llvm::CmpInst::Create(iop, pred, lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
    }
    else if (isFloat)
    {
        if (opr->IsOp("<"))
        {
            pred = llvm::FCmpInst::FCMP_OLT;
        }
        else if (opr->IsOp("<="))
        {
            pred = llvm::FCmpInst::FCMP_OLE;
        }
        else if (opr->IsOp(">"))
        {
            pred = llvm::FCmpInst::FCMP_OGT;
        }
        else if (opr->IsOp(">="))
        {
            pred = llvm::FCmpInst::FCMP_OGE;
        }
        return llvm::CmpInst::Create(iop, pred, lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
    }
}

llvm::Value* EqualityExpr::Emit()
{
    //Grab the operator
    Operator* opr = this->op;

    //Initialize Variables
    llvm::Value* lhs = this->left->Emit();
    llvm::Value* rhs = this->right->Emit(); 
    llvm::Type* lty = lhs->getType(); 
    llvm::Type* rty = rhs->getType();
    
    //Check if we're comparing ints or floats
    bool isInt = lty->isIntegerTy() && rty->isIntegerTy();
    bool isFloat = lty->isFloatTy() && rty->isFloatTy();

    llvm::CmpInst::OtherOps iop = llvm::CmpInst::ICmp;
    llvm::CmpInst::OtherOps fop = llvm::CmpInst::FCmp;

    llvm::CmpInst::Predicate pred;

    if (isInt)
    {
        if (opr->IsOp("=="))
        {
            pred = llvm::ICmpInst::ICMP_EQ;
        }
        else if (opr->IsOp("!="))
        {
            pred = llvm::ICmpInst::ICMP_NE;
        }
        return llvm::CmpInst::Create(iop, pred, lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
    }
    else if (isFloat)
    {
        if (opr->IsOp("=="))
        {
            pred = llvm::FCmpInst::FCMP_OEQ;
        }
        else if (opr->IsOp("!="))
        {
            pred = llvm::FCmpInst::FCMP_ONE;
        }
        return llvm::CmpInst::Create(iop, pred, lhs, rhs, opr->GetToken(), irgen->GetBasicBlock());
    }
}

llvm::Value* AssignExpr::Emit()
{
    //Grab the operator
    Operator* opr = this->op;

    //Initialize Variables
    llvm::Value* lhs = this->left->Emit();
    llvm::Value* rhs = this->right->Emit();
    llvm::Type* rty = rhs->getType();
    llvm::Type* lty = lhs->getType();

    //Set lhsType to INTEGER or FLOAT for unary operations
   /* bool rhsType;
    if (rty->isIntegerTy())
    {
        rhsType = INTEGER;
    }
    else if (rty->isFloatTy())
    {
        rhsType = FLOAT;
    } */

	//dynamic_cast to check if the left hand side expr is a field access
    FieldAccess* lfa = dynamic_cast<FieldAccess*>(this->left);
    FieldAccess* rfa = dynamic_cast<FieldAccess*>(this->right);

    //dynamic_cast to check if lhs expr is an array access
    ArrayAccess* laa = dynamic_cast<ArrayAccess*>(this->left);
    ArrayAccess* raa = dynamic_cast<ArrayAccess*>(this->right);

    //Gets the field access string
    char *fval;
    
    llvm::Value* eval; //Vector returned from vector arithmetic for assignment 
    VarExpr* base;  //Base expr
    llvm::Value* var;
    llvm::Value* vec;
    
    //Load the vector from the address recorded on the symtable
    if (laa == NULL)
    {
        if (lfa != NULL)
        { 
            fval = lfa->getField()->GetName();
            base = dynamic_cast<VarExpr*>(lfa->getBase());
        }
        else
        {
            base = dynamic_cast<VarExpr*>(this->left);
        }
        var = symtab->lookupAll(base->GetIdentifier()->GetName())->val;
        vec = new llvm::LoadInst(var, "", irgen->GetBasicBlock());
    }

    /*** Do basic assignment ***/
    if (opr->IsOp("="))
    {
        //If left hand side (lfa) is a field access, do assign for field access
        if (lfa != NULL)
        {
            //If rhs is a float, do float to vector field access assignment
            //else if the rhs is a field access, assign vector to vector
            if (rty->isFloatTy())
            {
                vec = llvm::InsertElementInst::Create(vec, rhs, Indexer(fval[0]), "", 
                                                irgen->GetBasicBlock());
            }
            else
            { 
                llvm::Value* rindx; //index for rfa (right field access)
                llvm::Value* elem;  //extracted element from rhs
                
                //For each field index on the rhs, insert into the vector at the
                //correct location on the lhs.
                int fieldLen = strlen(fval);
                int i;
                for (i = 0; i < fieldLen; i++)
                {
                    rindx = llvm::ConstantInt::get(irgen->GetIntType(), i);
                    elem = llvm::ExtractElementInst::Create(rhs, rindx, "", irgen->GetBasicBlock());
                    vec = llvm::InsertElementInst::Create(vec, elem, Indexer(fval[i]), 
                                                        "", irgen->GetBasicBlock()); 
                }
            }
            new llvm::StoreInst(vec, var, irgen->GetBasicBlock());
            return vec;
        }
        //assignment for arrays
		else if (laa != NULL)
		{
			llvm::BasicBlock* bb = irgen->GetBasicBlock();
		
			VarExpr* vexpr = dynamic_cast<VarExpr*>(laa->getBase());
			char* name = vexpr->GetIdentifier()->GetName();
			llvm::Value* bptr = (symtab->lookupAll(string(name)))->val;

			//emit on subsript
			llvm::Value* sexpr = laa->getSub()->Emit();

			//create array ref for instr
			std::vector<llvm::Value*> avec;
			avec.push_back(llvm::ConstantInt::get(Node::irgen->GetInt64Type(), 0));
			avec.push_back(sexpr);
			llvm::ArrayRef<llvm::Value *> aref(avec);
	
			llvm::Value* ptr = llvm::GetElementPtrInst::Create(bptr, aref, name, bb);
			
			new llvm::StoreInst(rhs, ptr, bb);
			return rhs;		
		}
        else 
        {
            //If the left side is just a vector variable, load it and assign all
            //its values the float value on the right hand side. 
            if (lty->isVectorTy() && rty->isFloatTy())
            {
                llvm::Value* indx;
                int i;
                llvm::Type* ty = lhs->getType();
                llvm::VectorType* vecTy = llvm::cast<llvm::VectorType>(ty);
                int numElem = vecTy->getNumElements();
                for (i = 0; i < numElem; i++)
                {
                    indx = llvm::ConstantInt::get(irgen->GetIntType(), i);
                    vec = llvm::InsertElementInst::Create(vec, rhs, indx, "",
                                                        irgen->GetBasicBlock());
                }
                rhs = vec;
            }
			//VarExpr* leftt = dynamic_cast<VarExpr*>(this->left);
            //llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;
            new llvm::StoreInst(rhs, var, irgen->GetBasicBlock());
            return rhs;
        }
    }
	//cerr << "0: op is: " << opr->GetToken() << "\n";
    else if (rty->isIntegerTy())
    {
        if (opr->IsOp("+="))
        {
            llvm::Value* eval = llvm::BinaryOperator::CreateAdd(lhs, rhs, "+", irgen->GetBasicBlock());
			VarExpr* leftt = dynamic_cast<VarExpr *>(this->left);
            llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;        
            new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
            return eval;
        }
        else if (opr->IsOp("-="))
        {
            llvm::Value* eval = llvm::BinaryOperator::CreateSub(lhs, rhs, "-", irgen->GetBasicBlock());
			VarExpr* leftt = dynamic_cast<VarExpr *>(this->left);
            llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;        
            new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
            return eval;
        }
        else if (opr->IsOp("*="))
        {
            llvm::Value* eval = llvm::BinaryOperator::CreateMul(lhs, rhs, "*", irgen->GetBasicBlock());
			VarExpr* leftt = dynamic_cast<VarExpr *>(this->left);
            llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;        
            new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
            return eval;
        }
        else if (opr->IsOp("/="))
        {
            llvm::Value* eval = llvm::BinaryOperator::CreateSDiv(lhs, rhs, "/", irgen->GetBasicBlock());
			VarExpr* leftt = dynamic_cast<VarExpr *>(this->left);
            llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;        
            new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
            return eval;
        }
    }
	//cerr << "2: op is: " << opr->GetToken() << "\n";
    else if (rty->isFloatTy())
    {
        if (lty->isVectorTy())
        {
            llvm::Type* ty = lhs->getType();
            llvm::VectorType* vecTy = llvm::cast<llvm::VectorType>(ty);
            llvm::Constant* floatr = llvm::cast<llvm::Constant>(rhs);
            llvm::Constant* newVec = llvm::ConstantVector::getSplat(
                                                vecTy->getNumElements(), floatr);
            if (opr->IsOp("+="))
            {
                llvm::Value* eval = llvm::BinaryOperator::CreateFAdd(lhs, newVec, "+", irgen->GetBasicBlock());
                VarExpr* leftt = dynamic_cast<VarExpr *>(this->left);
                llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;        
                new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
                return eval;
            }
            else if (opr->IsOp("-="))
            {
                llvm::Value* eval = llvm::BinaryOperator::CreateFSub(lhs, newVec, "-", irgen->GetBasicBlock());
                VarExpr* leftt = dynamic_cast<VarExpr *>(this->left);
                llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;        
                new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
                return eval;
            }
            else if (opr->IsOp("*="))
            {
                llvm::Value* eval = llvm::BinaryOperator::CreateFMul(lhs, newVec, "*", irgen->GetBasicBlock());
                VarExpr* leftt = dynamic_cast<VarExpr *>(this->left);
                llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;        
                new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
                return eval;
            }
            else if (opr->IsOp("/="))
            {
                llvm::Value* eval = llvm::BinaryOperator::CreateFDiv(lhs, newVec, "/", irgen->GetBasicBlock());
                VarExpr* leftt = dynamic_cast<VarExpr *>(this->left);
                llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;        
                new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
                return eval;
            }
        }

        if (opr->IsOp("+="))
        {
            llvm::Value* eval = llvm::BinaryOperator::CreateFAdd(lhs, rhs, "+", irgen->GetBasicBlock());
			VarExpr* leftt = dynamic_cast<VarExpr *>(this->left);
            llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;        
            new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
            return eval;
        }
        else if (opr->IsOp("-="))
        {
            llvm::Value* eval = llvm::BinaryOperator::CreateFSub(lhs, rhs, "-", irgen->GetBasicBlock());
			VarExpr* leftt = dynamic_cast<VarExpr *>(this->left);
            llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;        
            new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
            return eval;
        }
        else if (opr->IsOp("*="))
        {
            llvm::Value* eval = llvm::BinaryOperator::CreateFMul(lhs, rhs, "*", irgen->GetBasicBlock());
			VarExpr* leftt = dynamic_cast<VarExpr *>(this->left);
            llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;        
            new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
            return eval;
        }
        else if (opr->IsOp("/="))
        {
            llvm::Value* eval = llvm::BinaryOperator::CreateFDiv(lhs, rhs, "/", irgen->GetBasicBlock());
			VarExpr* leftt = dynamic_cast<VarExpr *>(this->left);
            llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;        
            new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
            return eval;
        }
    }
	//cerr << "3: op is: " << opr->GetToken() << "\n";
	else if (lty->isVectorTy() || rty->isVectorTy())
    {
        //cerr << "4: op is: " << opr->GetToken() << "\n";
        lhs = this->left->Emit();

        if (lfa != NULL)
        {
            if (opr->IsOp("+="))
            {
                eval = llvm::BinaryOperator::CreateFAdd(lhs, rhs, "+", irgen->GetBasicBlock());
                llvm::Value* swizVec = Swizzler(fval, vec, eval, rfa);
                new llvm::StoreInst(swizVec, var, irgen->GetBasicBlock());
                return swizVec;
            }
            else if (opr->IsOp("-="))
            {
                eval = llvm::BinaryOperator::CreateFSub(lhs, rhs, "+", irgen->GetBasicBlock());
                llvm::Value* swizVec = Swizzler(fval, vec, eval, rfa);
                new llvm::StoreInst(swizVec, var, irgen->GetBasicBlock());
                return swizVec;
            }
            else if (opr->IsOp("*="))
            {
                eval = llvm::BinaryOperator::CreateFMul(lhs, rhs, "+", irgen->GetBasicBlock());
                llvm::Value* swizVec = Swizzler(fval, vec, eval, rfa);
                new llvm::StoreInst(swizVec, var, irgen->GetBasicBlock());
                return swizVec;
            }
            else if (opr->IsOp("/="))
            {
                eval = llvm::BinaryOperator::CreateFDiv(lhs, rhs, "+", irgen->GetBasicBlock());
                llvm::Value* swizVec = Swizzler(fval, vec, eval, rfa);
                new llvm::StoreInst(swizVec, var, irgen->GetBasicBlock());
                return swizVec;
            }
        }
        else
        {
            if (opr->IsOp("+="))
            {
                //cerr << "5: op is: " << opr->GetToken() << "\n";
                llvm::Value* eval = llvm::BinaryOperator::CreateFAdd(lhs, rhs, "+", irgen->GetBasicBlock());
                VarExpr* leftbase = dynamic_cast<VarExpr*>(this->left);
                llvm::Value* var = symtab->lookupAll(leftbase->GetIdentifier()->GetName())->val;        
                new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
                return eval;
            }
            else if (opr->IsOp("-="))
            {
                llvm::Value* eval = llvm::BinaryOperator::CreateFSub(lhs, rhs, "-", irgen->GetBasicBlock());
                VarExpr* leftbase = dynamic_cast<VarExpr*>(this->left);
                llvm::Value* var = symtab->lookupAll(leftbase->GetIdentifier()->GetName())->val;        
                new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
                return eval;
            }
            else if (opr->IsOp("*="))
            {
                llvm::Value* eval = llvm::BinaryOperator::CreateFMul(lhs, rhs, "*", irgen->GetBasicBlock());
                VarExpr* leftbase = dynamic_cast<VarExpr*>(this->left);
                llvm::Value* var = symtab->lookupAll(leftbase->GetIdentifier()->GetName())->val;        
                new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
                return eval;
            }
            else if (opr->IsOp("/="))
            {
                llvm::Value* eval = llvm::BinaryOperator::CreateFDiv(lhs, rhs, "/", irgen->GetBasicBlock());
                VarExpr* leftbase = dynamic_cast<VarExpr*>(this->left);
                llvm::Value* var = symtab->lookupAll(leftbase->GetIdentifier()->GetName())->val;        
                new llvm::StoreInst(eval, var, irgen->GetBasicBlock());   
                return eval;
            } 
        }
    }
	return NULL;
}

llvm::Value* PostfixExpr::Emit()
{
    //Grab the operator
    Operator *opr = this->op;

    //Declare variables
    llvm::Value* lhs;
    llvm::Value* temp;
    llvm::Type* lty;
    llvm::Type* tty;

    //Grab the values.
    //Get their types
    //Then check if those types are integer or float types
    lhs = this->left->Emit(); 
    lty = lhs->getType();

    //Set lhsType to INTEGER or FLOAT for unary operations
    /*bool lhsType;
    if (lty->isIntegerTy())
    {
        lhsType = INTEGER;
    }
    else if (lty->isFloatTy())
    {
        lhsType = FLOAT;
    }*/
 
    if (lty->isIntegerTy())
    {
        if (opr->IsOp("++"))
        {
            temp = llvm::ConstantInt::get(irgen->GetIntType(), 1);
            llvm::Value* inc = llvm::BinaryOperator::CreateAdd(lhs, temp, opr->GetToken(), 
                                            irgen->GetBasicBlock());
			VarExpr* leftt = dynamic_cast<VarExpr *>(this->left); 
            llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;
            new llvm::StoreInst(inc, var, irgen->GetBasicBlock());
            return lhs;
        }
        else if (opr->IsOp("--"))
        {
            temp = llvm::ConstantInt::get(irgen->GetIntType(), 1);
            llvm::Value* dec = llvm::BinaryOperator::CreateSub(lhs, temp, opr->GetToken(), 
                                            irgen->GetBasicBlock());
			VarExpr* leftt = dynamic_cast<VarExpr *>(this->left);  
            llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;
            new llvm::StoreInst(dec, var, irgen->GetBasicBlock());
            return lhs;
        }
    }
    else if (lty->isFloatTy() || lty->isVectorTy())
    {
        if (opr->IsOp("++"))
        {
            temp = llvm::ConstantFP::get(lty, 1.0);
            llvm::Value* inc = llvm::BinaryOperator::CreateFAdd(lhs, temp, opr->GetToken(), 
                                            irgen->GetBasicBlock());
			VarExpr* leftt = dynamic_cast<VarExpr *>(this->left);
            llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;
            new llvm::StoreInst(inc, var, irgen->GetBasicBlock());
            return lhs;    
        }
        else if (opr->IsOp("--"))
        {
            temp = llvm::ConstantFP::get(lty, 1.0);
            llvm::Value* dec = llvm::BinaryOperator::CreateFSub(lhs, temp, opr->GetToken(), 
                                            irgen->GetBasicBlock());
			VarExpr* leftt = dynamic_cast<VarExpr *>(this->left);
            llvm::Value* var = symtab->lookupAll(leftt->GetIdentifier()->GetName())->val;
            new llvm::StoreInst(dec, var, irgen->GetBasicBlock());
            return lhs;
        }       
	}
}

llvm::Value* ConditionalExpr::Emit() {
	//get curr bb
	llvm::BasicBlock* bb = irgen->GetBasicBlock();

	//emit on condition
	llvm::Value* cond = this->cond->Emit();

	//emit on true expr
	llvm::Value* tru = this->trueExpr->Emit();

	//emit on false expr
	llvm::Value* fals = this->falseExpr->Emit();
	
	//create conditional instruciton
	llvm::SelectInst* inst = llvm::SelectInst::Create(cond, tru, fals, "", bb);

	return inst;
}

llvm::Value* ArrayAccess::Emit() {

	//get curr bb
	llvm::BasicBlock* bb = irgen->GetBasicBlock();

	//emit on base, get its pointer from symbol table
	llvm::Value* bexpr = this->base->Emit();
	VarExpr* vexpr = dynamic_cast<VarExpr*>(this->base);
	char* name = vexpr->GetIdentifier()->GetName();
	llvm::Value* bptr = (symtab->lookupAll(string(name)))->val;
	//llvm::Type* bty = bptr->getType();

	//emit on subsript
	llvm::Value* sexpr = this->subscript->Emit();

	

	//return llvm::ExtractElementInst::Create(bexpr, sexpr, name, bb);
	
	//create array ref for instr
	std::vector<llvm::Value*> avec;
	avec.push_back(llvm::ConstantInt::get(Node::irgen->GetInt64Type(), 0));
	avec.push_back(sexpr);
	llvm::ArrayRef<llvm::Value *> aref(avec);
	
	llvm::Value* ptr = llvm::GetElementPtrInst::Create(bptr, aref, name, bb);
	return new llvm::LoadInst(ptr, name, irgen->GetBasicBlock());
}

llvm::Value* FieldAccess::Emit()
{
    llvm::Value* bexpr = this->base->Emit();
    char *fval = this->field->GetName();
    int idxfield;
    int fieldLen = strlen(fval);
    
    if (fieldLen == 1)
    {
        return llvm::ExtractElementInst::Create(bexpr, Indexer(fval[0]), "", irgen->GetBasicBlock());
    }
    else
    {
        //Create mask for shufflevector
        vector<llvm::Constant*> mask;
        
        //Loop over swizzle and add indexes to mask
        int i;
        for(i = 0; i < fieldLen; i++)
        {
			llvm::Value* Index = Indexer(fval[i]);
			llvm::Constant* maskIndex = llvm::cast<llvm::Constant>(Index);
            mask.push_back(maskIndex);
        }
        llvm::ArrayRef<llvm::Constant *> maskArrayRef(mask);
        llvm::Constant* maskSwiz = llvm::ConstantVector::get(maskArrayRef);
        return new llvm::ShuffleVectorInst(bexpr, llvm::UndefValue::get(bexpr->getType()),
                                    maskSwiz, "", irgen->GetBasicBlock());
    }
    
    return NULL;
}

llvm::Value* Call::Emit() {

	//get curr bb
	llvm::BasicBlock* bb = irgen->GetBasicBlock();

	//get function and its name
	char* name = this->field->GetName();
	struct Data* funcData = symtab->lookupAll(string(name));
	llvm::Value* f = funcData->val;

	//get actuals
	std::vector<llvm::Value *> actualVals;
	int i;
	for(i = 0; i < this->actuals->NumElements(); i++)
	{
		llvm::Value* actualVal = this->actuals->Nth(i)->Emit();
		actualVals.push_back(actualVal);
	}
	llvm::ArrayRef<llvm::Value *> actualsArr(actualVals);

	//create instr for call
	llvm::CallInst* call = llvm::CallInst::Create(f, actualsArr, name, bb);

	return call;
}

//Passes in the vector to swizzle, rhs
//Returns the swizzled vector for storing.
llvm::Value* Swizzler(char* fval, llvm::Value* vec, llvm::Value* rhs, FieldAccess* fa)
{   
    llvm::Value* rindx;
    llvm::Value* elem;
    llvm::Value* eval;
    
    int swizLen;

    if (fa != NULL)
    {
        swizLen = strlen(fa->getField()->GetName());
    }
    else
    {
        swizLen = strlen(fval);
    }

    int i;
    for (i = 0; i < swizLen; i++)
    {
        rindx = llvm::ConstantInt::get(Node::irgen->GetIntType(), i);
        elem = llvm::ExtractElementInst::Create(rhs, rindx, "", Node::irgen->GetBasicBlock());
        vec = llvm::InsertElementInst::Create(vec, elem, Indexer(fval[i]), "",
                                        Node::irgen->GetBasicBlock());
    }
    return vec;
}

//Returns the converted index for the field access.
llvm::Value* Indexer(char fval)
{
    int idxfield;
    //Check field values for vector
    if (fval ==  'x')
    {
        idxfield = 0;
    }
    else if (fval ==  'y')
    {
        idxfield = 1;
    }
    else if (fval ==  'z') 
    {
        idxfield = 2;
    }
    else if (fval ==  'w') 
    {
        idxfield = 3;
    }
    return llvm::ConstantInt::get(Node::irgen->GetIntType(), idxfield);
}


IntConstant::IntConstant(yyltype loc, int val) : Expr(loc) {
    value = val;
}
void IntConstant::PrintChildren(int indentLevel) { 
    printf("%d", value);
}

FloatConstant::FloatConstant(yyltype loc, double val) : Expr(loc) {
    value = val;
}
void FloatConstant::PrintChildren(int indentLevel) { 
    printf("%g", value);
}

BoolConstant::BoolConstant(yyltype loc, bool val) : Expr(loc) {
    value = val;
}
void BoolConstant::PrintChildren(int indentLevel) { 
    printf("%s", value ? "true" : "false");
}

VarExpr::VarExpr(yyltype loc, Identifier *ident) : Expr(loc) {
    Assert(ident != NULL);
    this->id = ident;
}

void VarExpr::PrintChildren(int indentLevel) {
    id->Print(indentLevel+1);
}

Operator::Operator(yyltype loc, const char *tok) : Node(loc) {
    Assert(tok != NULL);
    strncpy(tokenString, tok, sizeof(tokenString));
}

void Operator::PrintChildren(int indentLevel) {
    printf("%s",tokenString);
}

bool Operator::IsOp(const char *op) const {
    return strcmp(tokenString, op) == 0;
}

CompoundExpr::CompoundExpr(Expr *l, Operator *o, Expr *r) 
  : Expr(Join(l->GetLocation(), r->GetLocation())) {
    Assert(l != NULL && o != NULL && r != NULL);
    (op=o)->SetParent(this);
    (left=l)->SetParent(this); 
    (right=r)->SetParent(this);
}

CompoundExpr::CompoundExpr(Operator *o, Expr *r) 
  : Expr(Join(o->GetLocation(), r->GetLocation())) {
    Assert(o != NULL && r != NULL);
    left = NULL; 
    (op=o)->SetParent(this);
    (right=r)->SetParent(this);
}

CompoundExpr::CompoundExpr(Expr *l, Operator *o) 
  : Expr(Join(l->GetLocation(), o->GetLocation())) {
    Assert(l != NULL && o != NULL);
    (left=l)->SetParent(this);
    (op=o)->SetParent(this);
}

void CompoundExpr::PrintChildren(int indentLevel) {
   if (left) left->Print(indentLevel+1);
   op->Print(indentLevel+1);
   if (right) right->Print(indentLevel+1);
}
   
ConditionalExpr::ConditionalExpr(Expr *c, Expr *t, Expr *f)
  : Expr(Join(c->GetLocation(), f->GetLocation())) {
    Assert(c != NULL && t != NULL && f != NULL);
    (cond=c)->SetParent(this);
    (trueExpr=t)->SetParent(this);
    (falseExpr=f)->SetParent(this);
}

void ConditionalExpr::PrintChildren(int indentLevel) {
    cond->Print(indentLevel+1, "(cond) ");
    trueExpr->Print(indentLevel+1, "(true) ");
    falseExpr->Print(indentLevel+1, "(false) ");
}
ArrayAccess::ArrayAccess(yyltype loc, Expr *b, Expr *s) : LValue(loc) {
    (base=b)->SetParent(this); 
    (subscript=s)->SetParent(this);
}

void ArrayAccess::PrintChildren(int indentLevel) {
    base->Print(indentLevel+1);
    subscript->Print(indentLevel+1, "(subscript) ");
}
     
FieldAccess::FieldAccess(Expr *b, Identifier *f) 
  : LValue(b? Join(b->GetLocation(), f->GetLocation()) : *f->GetLocation()) {
    Assert(f != NULL); // b can be be NULL (just means no explicit base)
    base = b; 
    if (base) base->SetParent(this); 
    (field=f)->SetParent(this);
}


void FieldAccess::PrintChildren(int indentLevel) {
    if (base) base->Print(indentLevel+1);
    field->Print(indentLevel+1);
}

Call::Call(yyltype loc, Expr *b, Identifier *f, List<Expr*> *a) : Expr(loc)  {
    Assert(f != NULL && a != NULL); // b can be be NULL (just means no explicit base)
    base = b;
    if (base) base->SetParent(this);
    (field=f)->SetParent(this);
    (actuals=a)->SetParentAll(this);
}

void Call::PrintChildren(int indentLevel) {
   if (base) base->Print(indentLevel+1);
   if (field) field->Print(indentLevel+1);
   if (actuals) actuals->PrintAll(indentLevel+1, "(actuals) ");
}

