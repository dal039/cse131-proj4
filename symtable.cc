/*
 * Symbol table implementation
 *
 */

#include "symtable.h"

using namespace std;

//Constructor for symbol table
SymbolTable::SymbolTable()
{
    scopesVec = new vector <map <string, struct Data*> >();
    currScopeCnt = -1;
}

//Push scope onto symbol table
void SymbolTable::pushScope()
{
    map <string, struct Data*> scope;
    scopesVec->push_back(scope);
    currScopeCnt++;
}

//Remove scope from symbol table
void SymbolTable::popScope()
{
    scopesVec->pop_back();
    currScopeCnt--;
}

//Add symbol to map on current scope
void SymbolTable::addSymbol(string id, Decl* node, llvm::Value* val)
{
	struct Data* data = (Data*)malloc(sizeof(*data));;
	data->decl = node;
	data->val = val;
    scopesVec->at(currScopeCnt).insert(pair<string, struct Data*>(id,data));
}

//Lookup symbol on map of current scope, returns Decl pointer
//Else return NULL
struct Data* SymbolTable::lookupCurr(string id)
{
    //If key, value pair exists, return value
    if (scopesVec->at(currScopeCnt).count(id) > 0)
    {
        return scopesVec->at(currScopeCnt)[id];
    }
    return NULL;
}

//Lookup symbol on entire symbol table, returns Decl pointer
//Else return NULL
struct Data* SymbolTable::lookupAll(string id)
{
    //If key, value pair exists anywhere in parent maps, return value
    for (int i  = currScopeCnt; i >= 0; i--)
    {
        if (scopesVec->at(i).count(id) > 0)
        {
            return scopesVec->at(i)[id];
        }
    }
    return NULL;
}

//Print table
void SymbolTable::printTable()
{
    int i;
    //iterate through maps
    for (i = 0; i <= currScopeCnt; i++)
    {
        map<string, struct Data*> thismap = scopesVec->at(i);
        map<string, struct Data*>::const_iterator it;
        for (it = thismap.begin(); it != thismap.end(); it++)
        {	
            cout << "Id: " << it->first << " Value: " << it->second << '\n';
			it->second->decl->GetPrintNameForNode();
        }
    }
}

bool SymbolTable::isGlobal(string s)
{
	if(currScopeCnt == 0) {
		return true;
	}
	return false;
}
