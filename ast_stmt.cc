/* File: ast_stmt.cc
 * -----------------
 * Implementation of statement node classes.
 */
#include "ast_stmt.h"
#include "ast_type.h"
#include "ast_decl.h"
#include "ast_expr.h"
#include "symtable.h"

#include "irgen.h"
#include "llvm/Bitcode/ReaderWriter.h"
#include "llvm/Support/raw_ostream.h"
#include <string>
#include <iostream>                                                 


Program::Program(List<Decl*> *d) {
    Assert(d != NULL);
    (decls=d)->SetParentAll(this);
}

void Program::PrintChildren(int indentLevel) {
    decls->PrintAll(indentLevel+1);
    printf("\n");
}

llvm::Value* Program::Emit() {
	//get module
    llvm::Module *mod = Node::irgen->GetOrCreateModule("Name_the_Module.bc");
	
	//push scope
	Node::symtab->pushScope();
	
	//loop over declarations
	if ( decls->NumElements() > 0 ) {
      for ( int i = 0; i < decls->NumElements(); ++i ) {
        Decl *d = decls->Nth(i);
        d->Emit();
      }
    }

	//mod->dump();

	//write bit code
	llvm::WriteBitcodeToFile(mod, llvm::outs());

	return NULL;
	
	/*
    // create a function signature
    std::vector<llvm::Type *> argTypes;
    llvm::Type *intTy = irgen.GetIntType();
    argTypes.push_back(intTy);
    llvm::ArrayRef<llvm::Type *> argArray(argTypes);
    llvm::FunctionType *funcTy = llvm::FunctionType::get(intTy, argArray, false);

    // llvm::Function *f = llvm::cast<llvm::Function>(mod->getOrInsertFunction("foo", intTy, intTy, (Type *)0));
    llvm::Function *f = llvm::cast<llvm::Function>(mod->getOrInsertFunction("foo", funcTy));
    llvm::Argument *arg = f->arg_begin();
    arg->setName("x");

    // insert a block into the runction
    llvm::LLVMContext *context = irgen.GetContext();
    llvm::BasicBlock *bb = llvm::BasicBlock::Create(*context, "entry", f);

    // create a return instruction
    llvm::Value *val = llvm::ConstantInt::get(intTy, 1);
    llvm::Value *sum = llvm::BinaryOperator::CreateAdd(arg, val, "", bb);
    llvm::ReturnInst::Create(*context, sum, bb);

    // write the BC into standard output
    
    llvm::WriteBitcodeToFile(mod, llvm::outs());
	*/
}

StmtBlock::StmtBlock(List<VarDecl*> *d, List<Stmt*> *s) {
    Assert(d != NULL && s != NULL);
    (decls=d)->SetParentAll(this);
    (stmts=s)->SetParentAll(this);
}

void StmtBlock::PrintChildren(int indentLevel) {
    decls->PrintAll(indentLevel+1);
    stmts->PrintAll(indentLevel+1);
}

llvm::Value* StmtBlock::Emit()
{
	//push scope
	symtab->pushScope();

    //call emit on stmts
    if ( stmts->NumElements() > 0 ) {
      for ( int i = 0; i < stmts->NumElements(); ++i ) {
        Stmt *s = stmts->Nth(i);
        s->Emit();
      }
    }

	//pop scope
	symtab->popScope();

	return NULL;
}

DeclStmt::DeclStmt(Decl *d) {
    Assert(d != NULL);
    (decl=d)->SetParent(this);
}

void DeclStmt::PrintChildren(int indentLevel) {
    decl->Print(indentLevel+1);
}

llvm::Value* DeclStmt::Emit() {
	llvm::Value* val = this->decl->Emit();
	return val;
}

ConditionalStmt::ConditionalStmt(Expr *t, Stmt *b) { 
    Assert(t != NULL && b != NULL);
    (test=t)->SetParent(this); 
    (body=b)->SetParent(this);
}

ForStmt::ForStmt(Expr *i, Expr *t, Expr *s, Stmt *b): LoopStmt(t, b) { 
    Assert(i != NULL && t != NULL && b != NULL);
    (init=i)->SetParent(this);
    step = s;
    if ( s )
      (step=s)->SetParent(this);
}

void ForStmt::PrintChildren(int indentLevel) {
    init->Print(indentLevel+1, "(init) ");
    test->Print(indentLevel+1, "(test) ");
    if ( step )
      step->Print(indentLevel+1, "(step) ");
    body->Print(indentLevel+1, "(body) ");
}

llvm::Value* ForStmt::Emit() {
	//get context and current bb
	llvm::LLVMContext *context = irgen->GetContext();
	llvm::BasicBlock* cbb = irgen->GetBasicBlock();

	//create bb for footer
	llvm::BasicBlock* fbb = llvm::BasicBlock::Create(*context, "footer", irgen->GetFunction());
	irgen->SetFooter(fbb);
	bbstack->push_back(fbb);

	//create bb for step
	llvm::BasicBlock* sbb = llvm::BasicBlock::Create(*context, "step", irgen->GetFunction());
	irgen->SetContinue(sbb);
	bbstack->push_back(sbb);

	//create bb for body
	llvm::BasicBlock* bbb = llvm::BasicBlock::Create(*context, "body", irgen->GetFunction());
	bbstack->push_back(bbb);
	
	//create bb for header
	llvm::BasicBlock* hbb = llvm::BasicBlock::Create(*context, "header", irgen->GetFunction());
	bbstack->push_back(hbb);

	//emit for initialization
	llvm::Value* ival = this->init->Emit();

	//create branch from current bb to header
	llvm::BranchInst::Create(hbb, cbb);

	//pop header set as curbb and emit test
	hbb = bbstack->back();
	bbstack->pop_back();
	irgen->SetBasicBlock(hbb);

	//emit test
	llvm::Value* tval = this->test->Emit();

	//jump from header to step or footer depending on test
	llvm::BranchInst::Create(bbb, fbb, tval, hbb);

	//emit for body
	bbb = bbstack->back();
	bbstack->pop_back();
	irgen->SetBasicBlock(bbb);

	this->body->Emit();

	//check if body has term instr
	if( irgen->GetBasicBlock()->getTerminator() == NULL ) {
		llvm::BranchInst::Create(sbb, irgen->GetBasicBlock());
	}

	//emit for step
	sbb = bbstack->back();
	bbstack->pop_back();
	irgen->SetBasicBlock(sbb);

	this->step->Emit();

	//create term inst for step
	llvm::BranchInst::Create(hbb, sbb);

	//pop footer and set as currbb
	fbb = bbstack->back();
	bbstack->pop_back();
	irgen->SetBasicBlock(fbb);

	return NULL;

}

void WhileStmt::PrintChildren(int indentLevel) {
    test->Print(indentLevel+1, "(test) ");
    body->Print(indentLevel+1, "(body) ");
}

llvm::Value* WhileStmt::Emit() {
	
	llvm::LLVMContext *context = irgen->GetContext();
	llvm::BasicBlock* cbb = irgen->GetBasicBlock();

	//creat bb for footer
	llvm::BasicBlock* fbb = llvm::BasicBlock::Create(*context, "footer", irgen->GetFunction());
	irgen->SetFooter(fbb);
	bbstack->push_back(fbb);

	//create bb for body
	llvm::BasicBlock* bbb = llvm::BasicBlock::Create(*context, "body", irgen->GetFunction());
	bbstack->push_back(bbb);
	
	//create bb for header
	llvm::BasicBlock* hbb = llvm::BasicBlock::Create(*context, "header", irgen->GetFunction());
	irgen->SetContinue(hbb);
	bbstack->push_back(hbb);

	//create branch from current bb to header
	llvm::BranchInst::Create(hbb, cbb);

	//pop header set as curbb and emit test
	hbb = bbstack->back();
	bbstack->pop_back();
	irgen->SetBasicBlock(hbb);

	//emit test
	llvm::Value* tval = this->test->Emit();

	//jump from header to step or footer depending on test
	llvm::BranchInst::Create(bbb, fbb, tval, hbb);

	//emit for body
	bbb = bbstack->back();
	bbstack->pop_back();
	irgen->SetBasicBlock(bbb);

	this->body->Emit();

	//check if body has term instr
	if( irgen->GetBasicBlock()->getTerminator() == NULL ) {
		llvm::BranchInst::Create(hbb, irgen->GetBasicBlock());
	}

	//pop footer and set as currbb
	fbb = bbstack->back();
	bbstack->pop_back();
	irgen->SetBasicBlock(fbb);

	return NULL;
	
}

IfStmt::IfStmt(Expr *t, Stmt *tb, Stmt *eb): ConditionalStmt(t, tb) { 
    Assert(t != NULL && tb != NULL); // else can be NULL
    elseBody = eb;
    if (elseBody) elseBody->SetParent(this);
}

void IfStmt::PrintChildren(int indentLevel) {
    if (test) test->Print(indentLevel+1, "(test) ");
    if (body) body->Print(indentLevel+1, "(then) ");
    if (elseBody) elseBody->Print(indentLevel+1, "(else) ");
}

llvm::Value* IfStmt::Emit() {

	llvm::BasicBlock *ebb = NULL;

	llvm::LLVMContext *context = irgen->GetContext();
	llvm::BasicBlock* cbb = irgen->GetBasicBlock();

	//1. emit for test cond
	llvm::Value* tval = this->test->Emit();
	
	//2. create bb for footer and push?
	llvm::BasicBlock* tbb = llvm::BasicBlock::Create(*context, "then", irgen->GetFunction());
	
	//3.create bb for else
	if( this->elseBody != NULL ) {
		ebb = llvm::BasicBlock::Create(*context, "else", irgen->GetFunction());
	}

	//4. create bb for then
	llvm::BasicBlock* fbb = llvm::BasicBlock::Create(*context, "footer", irgen->GetFunction());

	//push bbs into stack
	bbstack->push_back(fbb);
	if( this->elseBody != NULL) {
		bbstack->push_back(ebb);
	}
	bbstack->push_back(tbb);

	//5. creat branch inst for then
	if( this->elseBody != NULL ) {
		llvm::BranchInst::Create(tbb, ebb, tval, cbb);
	}
	else {
		llvm::BranchInst::Create(tbb, fbb, tval, cbb);
	}

	//pop then, emit, creat branch to footer
	tbb = bbstack->back();
	bbstack->pop_back();
	irgen->SetBasicBlock(tbb);
	this->body->Emit();
	//TODO: Changed all the branch checks to branch from getbasicblock to whatev
	if( irgen->GetBasicBlock()->getTerminator() == NULL ) {
		llvm::BranchInst::Create(fbb, irgen->GetBasicBlock());
	}

	//pop else, emit, create branch to footer
	if( this->elseBody != NULL ) {
		ebb = bbstack->back();
		bbstack->pop_back();
		irgen->SetBasicBlock(ebb);
		this->elseBody->Emit();
		if( irgen->GetBasicBlock()->getTerminator() == NULL ) {
			llvm::BranchInst::Create(fbb, irgen->GetBasicBlock());
		}
	}

	//pop footer and set as curbb
	fbb = bbstack->back();
	bbstack->pop_back();
	irgen->SetBasicBlock(fbb);

	return NULL;
	
}


ReturnStmt::ReturnStmt(yyltype loc, Expr *e) : Stmt(loc) { 
    expr = e;
    if (e != NULL) expr->SetParent(this);
}

void ReturnStmt::PrintChildren(int indentLevel) {
    if ( expr ) 
      expr->Print(indentLevel+1);
}

llvm::Value* ReturnStmt::Emit() {
	
	llvm::BasicBlock* bb = irgen->GetBasicBlock();
	llvm::LLVMContext *context = irgen->GetContext();
	
	if( expr != NULL ) {
		llvm::Value* val = this->expr->Emit();
		llvm::ReturnInst::Create(*context, val, bb);
		return val;
	}

	else {
		llvm::ReturnInst::Create(*context, bb);
		return NULL;
	}

}

llvm::Value* BreakStmt::Emit() {
	//cerr << "in break\n";	

	llvm::BasicBlock* bb = irgen->GetBasicBlock();
	llvm::LLVMContext *context = irgen->GetContext();

	llvm::BasicBlock* fbb = irgen->GetFooter();

	llvm::BranchInst::Create(fbb, bb);
	
	return NULL;
	
}

llvm::Value* ContinueStmt::Emit() {
	
	llvm::BasicBlock* bb = irgen->GetBasicBlock();
	llvm::LLVMContext *context = irgen->GetContext();

	llvm::BasicBlock* cbb = irgen->GetContinue();

	llvm::BranchInst::Create(cbb, bb);

	return NULL;
	
}


SwitchLabel::SwitchLabel(Expr *l, Stmt *s) {
    Assert(l != NULL && s != NULL);
    (label=l)->SetParent(this);
    (stmt=s)->SetParent(this);
}

SwitchLabel::SwitchLabel(Stmt *s) {
    Assert(s != NULL);
    label = NULL;
    (stmt=s)->SetParent(this);
}

void SwitchLabel::PrintChildren(int indentLevel) {
    if (label) label->Print(indentLevel+1);
    if (stmt)  stmt->Print(indentLevel+1);
}
/*
Expr* Case::getLabel() { return this->label;}
Stmt* Case::getStmt() { return this->stmt;}
*/

Expr* SwitchLabel::getLabel() { return label;}
Stmt* SwitchLabel::getStmt() { return stmt;}

SwitchStmt::SwitchStmt(Expr *e, List<Stmt *> *c, Default *d) {
    Assert(e != NULL && c != NULL && c->NumElements() != 0 );
    (expr=e)->SetParent(this);
    (cases=c)->SetParentAll(this);
    def = d;
    if (def) def->SetParent(this);
}

void SwitchStmt::PrintChildren(int indentLevel) {
    if (expr) expr->Print(indentLevel+1);
    if (cases) cases->PrintAll(indentLevel+1);
    if (def) def->Print(indentLevel+1);
}

llvm::Value* SwitchStmt::Emit() {

	llvm::BasicBlock* cbb = irgen->GetBasicBlock();
	llvm::LLVMContext *context = irgen->GetContext();
	llvm::Module *mod = irgen->GetOrCreateModule("Name_the_Module.bc");

	//create bb for footer and push?
	llvm::BasicBlock* fbb = llvm::BasicBlock::Create(*context, "footer", irgen->GetFunction());
	irgen->SetFooter(fbb);
	bbstack->push_back(fbb);

	llvm::BasicBlock* dbb;
	SwitchLabel* df;
	cerr << "# elem: " << cases->NumElements() << "\n";

	int caseCnt = 0;
	//find all cases, inc. default, create BB for each
	for(int i = cases->NumElements()-1; i >= 0; i-- ) {
		 		

		SwitchLabel* c = dynamic_cast<SwitchLabel*>(this->cases->Nth(i));
		BreakStmt* b = dynamic_cast<BreakStmt*>(this->cases->Nth(i));

		if(c != NULL) { 
			if(c->getLabel() != NULL ) {
				cerr << "index: " << i << " is case " << "label: " << c->getLabel()->Emit() << "\n"; 
			}
			else if(c->getLabel() == NULL) {
				cerr << "index: " << i << " is def " << c << "\n";
			}
		} 

		if(b != NULL) { cerr << "index: " << i << " is break " << b << "\n"; }


		if( c != NULL ) {
			if( c->getLabel() == NULL ) {
				df = c;
				dbb = llvm::BasicBlock::Create(*context, "default", irgen->GetFunction());
				bbstack->push_back(dbb);
			}

			else {
				caseCnt++;
				llvm::BasicBlock* bb = llvm::BasicBlock::Create(*context, "case", irgen->GetFunction());
				bbstack->push_back(bb);
			}

		}
	}

	//mod->dump(); //TODO
	
	//emit for expr
	llvm::Value* eval = this->expr->Emit();

	//create switch instr TODO: make sure of the numelments/2  TODO
	llvm::SwitchInst* inst = llvm::SwitchInst::Create(eval, dbb, caseCnt, cbb);

	//for each case, add the case, then emit on stmt, and on its break
	for( int i = 0; i < cases->NumElements() ; i++ ) {

		SwitchLabel* c = dynamic_cast<SwitchLabel *>(this->cases->Nth(i));

		if( c != NULL ) {
			//get prev bb to support fall thru
			//llvm::BasicBlock* prevbb = irgen->GetBasicBlock();

			//get corresponding case's bb
			llvm::BasicBlock* bb = bbstack->back();
			bbstack->pop_back();
			irgen->SetBasicBlock(bb);
			
			//check if it is a case and not def, add to switch
			if( c->getLabel() != NULL ) {

				//get the case's value
				llvm::Value* labelval = (c->getLabel())->Emit();
				llvm::ConstantInt* v = llvm::cast<llvm::ConstantInt>(labelval);
					
				//add the case to switch
				inst->addCase(v,bb);
			}

			//emit the stmt inside the case
			c->getStmt()->Emit(); 
			
			//look for break stmt
			BreakStmt* brks = NULL;
			if( i != cases->NumElements()-1 ){

				//dynamic cast next elem to see if it is break
				Stmt* chkbrk = cases->Nth(i+1);
				brks = dynamic_cast<BreakStmt*>(chkbrk);
			
				//if it does have a break, emit it
				if( brks != NULL ){
					brks->Emit();
				}
				//if doesn't have a break, create branch to next case
				else {
					llvm::BasicBlock* nextCase = bbstack->back();
					llvm::BranchInst::Create(nextCase,bb);
				}
			}

			//if we are at the last element, and it is a case, then create branch to footer
			else {
				llvm::BranchInst::Create(fbb,bb);
			}
		}
	}

	//pop footer and set as currbb
	fbb = bbstack->back();
	bbstack->pop_back();
	irgen->SetBasicBlock(fbb);

	return eval; //TODO: is this what im supposed to return?
}

