; ModuleID = 'Checkpoint/switch_basic.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-redhat-linux-gnu"

@a = global i32 0

define float @switchtest() {
entry:
  %f = alloca float
  %a = load i32* @a
  switch i32 %a, label %default [
    i32 2, label %case2
    i32 1, label %case1
    i32 0, label %case
  ]

footer:                                           ; preds = %default, %case2, %case1, %case
  %f7 = load float* %f
  ret float %f7

case:                                             ; preds = %entry
  %f5 = load float* %f
  store float 0.000000e+00, float* %f
  br label %footer

case1:                                            ; preds = %entry
  %f4 = load float* %f
  store float 1.000000e+00, float* %f
  br label %footer

case2:                                            ; preds = %entry
  %f3 = load float* %f
  store float 2.000000e+00, float* %f
  br label %footer

default:                                          ; preds = %entry
  %f6 = load float* %f
  store float 3.000000e+00, float* %f
  br label %footer
}
